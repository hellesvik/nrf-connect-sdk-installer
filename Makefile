MANDIR      = $(DESTDIR)/usr/share/man/man1
LICENSEDIR  = $(DESTDIR)/usr/share/licenses/nrf-connect-sdk-installer
BINDIR      = $(DESTDIR)/usr/bin

PKGNAME     = nrf-connect-sdk-installer
SCRIPT      = $(PKGNAME).sh
MANPAGE     = $(PKGNAME).1.gz

install: $(MANPAGE)
	mkdir -p $(MANDIR)
	mkdir -p $(LICENSEDIR)
	mkdir -p $(BINDIR)
	chmod 644 $<
	chmod 644 LICENSE
	chmod 755 $(SCRIPT)
	cp $< $(MANDIR)/$<
	cp LICENSE $(LICENSEDIR)/LICENSE
	cp $(SCRIPT) $(BINDIR)/$(PKGNAME)

$(MANPAGE):
	help2man -n 'Shell script to easily control brightness.' \
		-N -h -h -v -v ./$(SCRIPT) | gzip - > $(MANPAGE)

uninstall:
	$(RM) -r $(LICENSEDIR)
	$(RM) $(MANDIR)/$(MANPAGE) $(BINDIR)/$(PKGNAME)

.PHONY: install uninstall

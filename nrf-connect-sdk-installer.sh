#!/usr/bin/bash
(

check_dependencies () {
    dependencies=( 
            'git'
            'cmake'
            'ninja'
            'gperf'
            'dtc'
            'python'
            'wheel'
            'xz'
            'west'
            'nrfjprog'
            )
    for dependency in "${dependencies[@]}"
        do
            if ! command -v $dependency &> /dev/null
            then    
                echo "Missing dependency for building nRF Connect SDK projects: $dependency"
            fi
        done

}
help_needed=true
if ! [[ -z $1 ]]; then
    if [ $1 = '--install-dir' ]  ; then
        help_needed=false
        install_success=false
        if [ -z $2 ]; then
            echo "ERROR: missing --install-dir"
        elif [ -d $2 ]; then
            if [ -d ${2}/nrf_connect_sdk ]; then
                echo "ERROR: Directory nrf_connect_sdk already exist."
            elif ! [[ -z $ZEPHYR_BASE ]]; then
                echo "ERROR: ZEPHYR_BASE env variable already exist."
            else 
                echo "Will install into ${2}/nrf_connect_sdk."
                echo "Tag number(Default v2.1.0): "
                read tag

                if [ -z ${tag} ]; then
                    tag="v2.1.0"
                fi
                if ! [[ ${tag:0:1} = "v" ]] ||  ! [[ ${tag:2:1} = "." ]] || ! [[ ${tag:4:1} = "." ]]; then
                    echo "Error in tag format."
                else
                    ncs=${2}/nrf_connect_sdk
                    mkdir ${ncs}
                    cd ${ncs}
                    west init -m https://github.com/nrfconnect/sdk-nrf --mr ${tag}
                    west update
                    west zephyr-export
                    install_success=true
                    check_dependencies
                fi
            fi

        else
            echo "ERROR: Not a valid directory"
        fi
        if [ $install_success = true ]; then
            echo "Set up environement variables? y/N"
            echo "(Copy zephyr-env.sh to .bashrc)"
            read setup_env
            if [ $setup_env = y ] || [ $setup_env = Y ]; then
                echo "source ${2}/nrf_connect_sdk/zephyr/zephyr-env.sh" >> ${HOME}/.bashrc
            fi
        fi
    fi
fi

if [ $help_needed = true ]; then
    echo ""
    echo "usage: nrf-connect-sdk-install-helper --install-dir <path>"
    echo ""
    echo "options:"
    echo "  --help: Display this dialog"
    echo "  --install-dir <path>: Set installation directory for the nRF Connect SDK."
    echo ""
fi
)

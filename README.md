# nrf-connect-sdk-installer
Automated helper script to install the nRF Connect SDK on Linux.


For now, this script assumes that you already have installed required packages, depencencies and the toolchain from your package manager.
This script will perform [Manual Install: 3 Get the nRF Connect SDK Code](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/gs_installing.html#get-the-ncs-code)


Tested on:
Arch Linux
